package it.unipi.di.sam.pacioni.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.unipi.di.sam.pacioni.R
import it.unipi.di.sam.pacioni.SearchAdapter
import it.unipi.di.sam.pacioni.network.Kind
import it.unipi.di.sam.pacioni.network.SoundCloudApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SearchFragment : Fragment(), SearchView.OnQueryTextListener {
    private lateinit var searchView: SearchView
    private lateinit var rvSearch: RecyclerView
    private val adapter = SearchAdapter(emptyList()) { item ->
        val fragment = when (item.kind) {
            Kind.track -> TrackFragment()
            Kind.user -> UserFragment()
            Kind.playlist -> PlaylistFragment()
            else -> null
        }
        fragment?.let {
            val name = item.kind.toString()
            parentFragmentManager.setFragmentResult("BUNDLE", bundleOf(name to item))
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.flFragment, fragment, name)
                addToBackStack(name)
                commit()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvSearch = view.findViewById(R.id.rvSearch)
        searchView = view.findViewById(R.id.searchView)

        rvSearch.adapter = adapter
        rvSearch.layoutManager = LinearLayoutManager(context)
        searchView.setOnQueryTextListener(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_search, container, false)

    override fun onQueryTextSubmit(query: String?): Boolean {
        query?.let {
            GlobalScope.launch(Dispatchers.IO) {
                val result = SoundCloudApi.retrofitService.search(50, it).collection ?: emptyList()
                GlobalScope.launch(Dispatchers.Main) {
                    if (result.isNotEmpty()) {
                        adapter.notifyDataSetChanged()
                    } else {
                        view?.findViewById<TextView>(R.id.tvNoResult)?.visibility = View.VISIBLE
                    }
                    adapter.setItems(result)
                }
            }
        }
        searchView.clearFocus()
        rvSearch.smoothScrollToPosition(0)
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        view?.findViewById<TextView>(R.id.tvNoResult)?.visibility = View.INVISIBLE
        // TODO update the query list
        return false
    }
}