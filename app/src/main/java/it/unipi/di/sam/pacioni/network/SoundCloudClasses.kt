package it.unipi.di.sam.pacioni.network

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

data class TrackURL(
    val url: String,
)

@JsonClass(generateAdapter = true)
data class SoundCloudSearchResponse(
    @Json(name = "collection")
    val collection: List<SoundCloudCollection>?,
    @Json(name = "next_href")
    val nextHref: String?,
    @Json(name = "total_results")
    val totalResults: Int?
)

enum class Kind(val i: Int) { track(0), user(1), playlist(2) }

@JsonClass(generateAdapter = true)
@Parcelize
data class SoundCloudCollection(
    @Json(name = "artwork_url")
    val artworkUrl: String?,
    @Json(name = "avatar_url")
    val avatarUrl: String?,
    val city: String?,
    @Json(name = "comment_count")
    val commentCount: Int?,
    @Json(name = "comments_count")
    val commentsCount: Int?,
    @Json(name = "country_code")
    val countryCode: String?,
    @Json(name = "created_at")
    val createdAt: String?,
    val description: String?,
    val duration: String?,
    @Json(name = "first_name")
    val firstName: String?,
    @Json(name = "followers_count")
    val followersCount: Int?,
    @Json(name = "followings_count")
    val followingsCount: Int?,
    @Json(name = "full_duration")
    val fullDuration: Int?,
    @Json(name = "full_name")
    val fullName: String?,
    val genre: String?,
    val id: Int,
    val kind: Kind?,
    @Json(name = "last_modified")
    val lastModified: String?,
    @Json(name = "last_name")
    val lastName: String?,
    @Json(name = "likes_count")
    val likesCount: Int?,
    @Json(name = "media")
    val media: Media?,
    @Json(name = "permalink")
    val permalink: String?,
    @Json(name = "permalink_url")
    val permalinkUrl: String?,
    @Json(name = "playback_count")
    val playbackCount: Int?,
    @Json(name = "playlist_count")
    val playlistCount: Int?,
    @Json(name = "playlist_likes_count")
    val playlistLikesCount: Int?,
    @Json(name = "release_date")
    val releaseDate: String?,
    @Json(name = "reposts_count")
    val repostsCount: Int?,
    @Json(name = "tag_list")
    val tagList: String?,
    val title: String?,
    val tracks: List<SoundCloudCollection>?,
    @Json(name = "track_count")
    val trackCount: Int?,
    @Json(name = "track_format")
    val trackFormat: String?,
    val uri: String?,
    val user: User?,
    @Json(name = "user_id")
    val userId: Int?,
    val username: String?,
    @Json(name = "waveform_url")
    val waveformUrl: String?
) : Parcelable

@Parcelize
@JsonClass(generateAdapter = true)
data class Media(
    @Json(name = "transcodings")
    val transcodings: List<Transcoding>
) : Parcelable {
    @Parcelize
    @JsonClass(generateAdapter = true)
    data class Transcoding(
        @Json(name = "duration")
        val duration: Int,
        @Json(name = "format")
        val format: Format,
        @Json(name = "preset")
        val preset: String,
        @Json(name = "quality")
        val quality: String,
        @Json(name = "snipped")
        val snipped: Boolean,
        @Json(name = "url")
        val url: String
    ) : Parcelable {
        @Parcelize
        @JsonClass(generateAdapter = true)
        data class Format(
            @Json(name = "mime_type")
            val mimeType: String,
            @Json(name = "protocol")
            val protocol: String
        ) : Parcelable
    }
}

@JsonClass(generateAdapter = true)
@Parcelize
data class Track(
    @Json(name = "artwork_url")
    val artworkUrl: String?,
    @Json(name = "caption")
    val caption: String?,
    @Json(name = "comment_count")
    val commentCount: Int?,
    @Json(name = "commentable")
    val commentable: Boolean?,
    @Json(name = "created_at")
    val createdAt: String?,
    @Json(name = "description")
    val description: String?,
    @Json(name = "display_date")
    val displayDate: String?,
    @Json(name = "download_count")
    val downloadCount: Int?,
    val downloadable: Boolean?,
    @Json(name = "duration")
    val duration: Int?,
    @Json(name = "embeddable_by")
    val embeddableBy: String?,
    @Json(name = "full_duration")
    val fullDuration: Int?,
    @Json(name = "genre")
    val genre: String?,
    @Json(name = "has_downloads_left")
    val hasDownloadsLeft: Boolean?,
    @Json(name = "id")
    val id: Int?,
    val kind: Kind?,
    @Json(name = "label_name")
    val labelName: String?,
    @Json(name = "last_modified")
    val lastModified: String?,
    @Json(name = "license")
    val license: String?,
    @Json(name = "likes_count")
    val likesCount: Int?,
    @Json(name = "media")
    val media: Media?,
    @Json(name = "monetization_model")
    val monetizationModel: String?,
    @Json(name = "permalink")
    val permalink: String?,
    @Json(name = "permalink_url")
    val permalinkUrl: String?,
    @Json(name = "playback_count")
    val playbackCount: Int?,
    @Json(name = "purchase_url")
    val purchaseUrl: String?,
    @Json(name = "release_date")
    val releaseDate: String?,
    @Json(name = "reposts_count")
    val repostsCount: Int?,
    @Json(name = "sharing")
    val sharing: String?,
    @Json(name = "state")
    val state: String?,
    @Json(name = "station_permalink")
    val stationPermalink: String?,
    @Json(name = "streamable")
    val streamable: Boolean?,
    @Json(name = "tag_list")
    val tagList: String?,
    @Json(name = "title")
    val title: String?,
    @Json(name = "track_format")
    val trackFormat: String?,
    @Json(name = "uri")
    val uri: String?,
    @Json(name = "urn")
    val urn: String?,
    @Json(name = "user")
    val user: User?,
    @Json(name = "user_id")
    val userId: Int?,
    @Json(name = "waveform_url")
    val waveformUrl: String?
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
data class User(
    @Json(name = "avatar_url")
    val avatarUrl: String,
    val city: String?,
    @Json(name = "comments_count")
    val commentsCount: Int?,
    @Json(name = "country_code")
    val countryCode: String?,
    @Json(name = "created_at")
    val createdAt: String?,
    @Json(name = "description")
    val description: String?,
    @Json(name = "first_name")
    val firstName: String?,
    @Json(name = "followers_count")
    val followersCount: Int?,
    @Json(name = "followings_count")
    val followingsCount: Int?,
    @Json(name = "full_name")
    val fullName: String?,
    @Json(name = "groups_count")
    val groupsCount: Int?,
    @Json(name = "id")
    val id: Int,
    @Json(name = "last_name")
    val lastName: String?,
    @Json(name = "likes_count")
    val likesCount: Int?,
    @Json(name = "permalink")
    val permalink: String,
    @Json(name = "permalink_url")
    val permalinkUrl: String,
    @Json(name = "playlist_count")
    val playlistCount: Int?,
    @Json(name = "playlist_likes_count")
    val playlistLikesCount: Int?,
    @Json(name = "reposts_count")
    val repostsCount: String?,
    @Json(name = "station_permalink")
    val stationPermalink: String,
    @Json(name = "track_count")
    val trackCount: Int?,
    @Json(name = "uri")
    val uri: String,
    @Json(name = "urn")
    val urn: String,
    val username: String?,
) : Parcelable