package it.unipi.di.sam.pacioni.utils

import android.content.Context
import android.os.SystemClock
import android.support.v4.media.session.PlaybackStateCompat
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.Fragment
import it.unipi.di.sam.pacioni.GlideApp
import it.unipi.di.sam.pacioni.R
import it.unipi.di.sam.pacioni.network.Media
import java.time.LocalTime

private  const val PROTOCOL = "progressive"
private  const val MIME_TYPE = "audio/mpeg"

object Utility {

    fun changeImageUrl(url: String?): String {
        return url?.replace("large", "t500x500") ?: ""
    }

    /**
     * https://api-v2.soundcloud.com/media/soundcloud:tracks:{ID}/{CODE}/stream/{TYPE}
     */
    fun extractMediaCode(url: String): String {
        return url.split("/")[5]
    }

    fun extractMediaType(url: String): String {
        return url.split("/").last()
    }

    fun loadImage(fragment: Fragment, url: String?, image: ImageView) {
        GlideApp.with(fragment)
            .load(changeImageUrl(url))
            .placeholder(R.drawable.ic_image_place_holder)
            .error(R.drawable.ic_broken_image)
            .fallback(R.drawable.ic_no_image)
            .into(image)
    }

    fun loadImage(view: View, url: String?, image: ImageView) {
        GlideApp.with(view)
            .load(changeImageUrl(url))
            .placeholder(R.drawable.ic_image_place_holder)
            .error(R.drawable.ic_broken_image)
            .fallback(R.drawable.ic_no_image)
            .into(image)
    }

    fun convertDuration(duration: String?): String {
        return LocalTime.ofSecondOfDay((duration?.toLong()?.div(1000) ?: 0)).toString()
    }

    // copied without ritegno from UAMP
    fun timestampToMSS(context: Context, position: Long): String {
        val totalSeconds = Math.floor(position / 1E3).toInt()
        val minutes = totalSeconds / 60
        val remainingSeconds = totalSeconds - (minutes * 60)
        return if (position < 0) context.getString(R.string.duration_unknown)
        else context.getString(R.string.duration_format).format(minutes, remainingSeconds)
    }

    fun chooseTranscoding(transcodings: List<Media.Transcoding>): Media.Transcoding {
        return try {
            transcodings.first { it.format.protocol == PROTOCOL }
        } catch (e: NoSuchElementException) {
            transcodings.first { it.format.mimeType == MIME_TYPE }
        }
    }

    // pure questa
    inline val PlaybackStateCompat.currentPlayBackPosition: Long
        get() = if (state == PlaybackStateCompat.STATE_PLAYING) {
            val timeDelta = SystemClock.elapsedRealtime() - lastPositionUpdateTime
            (position + (timeDelta * playbackSpeed)).toLong()
        } else {
            position
        }

}