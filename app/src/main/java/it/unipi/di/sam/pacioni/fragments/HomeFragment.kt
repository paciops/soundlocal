package it.unipi.di.sam.pacioni.fragments

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import it.unipi.di.sam.pacioni.R
import it.unipi.di.sam.pacioni.TracksAdapter
import it.unipi.di.sam.pacioni.network.Kind
import it.unipi.di.sam.pacioni.network.SoundCloudApi
import it.unipi.di.sam.pacioni.network.SoundCloudCollection
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    private val limit = 50
    private var offset = 0
    private lateinit var adapter: TracksAdapter
    private lateinit var tracks: List<SoundCloudCollection>
    private lateinit var trackView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // check if tracks are already in the bundle
        // otherwise set an empty list
        tracks = emptyList()

        savedInstanceState?.getParcelableArray(Kind.track.toString())
            ?.map { it as SoundCloudCollection }
            ?.let { tracks = it }

        adapter = TracksAdapter(tracks) {
            val trackFragment = TrackFragment()
            val name = Kind.track.toString()
            parentFragmentManager.setFragmentResult("BUNDLE", bundleOf(name to it))
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.flFragment, trackFragment, name)
                setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                addToBackStack(name)
                commit()
            }
        }
        // if tracks are empty retrieve tracks from api
        if (tracks.isEmpty()) {
            GlobalScope.launch(Dispatchers.IO) {
                tracks =
                    SoundCloudApi.retrofitService.allMusic(limit, offset).collection ?: emptyList()
                if (tracks.isNotEmpty()) {
                    GlobalScope.launch(Dispatchers.Main) {
                        adapter.setItems(tracks)
                        adapter.notifyDataSetChanged()
                    }
                } else {
                    TODO("implement empty list in home")
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_home, container, false)
        trackView = rootView.findViewById(R.id.rvTracks)
        trackView.adapter = adapter
        return rootView
    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        // onSaveInstanceState is not called and the tracks are reloaded
        requireActivity().recreate()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArray(Kind.track.toString(), tracks.toTypedArray())
    }
}
