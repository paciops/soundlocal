package it.unipi.di.sam.pacioni.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

private const val BASE_URL = "https://api-v2.soundcloud.com"
private const val CLIENT_ID = "a3e059563d7fd3372b49b37f00a00bcf"

/**
 * Build the Moshi object with Kotlin adapter factory that Retrofit will be using.
 */

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val client = OkHttpClient.Builder()
    .addInterceptor(AddClientId)
    .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
    .build()

/**
 * The Retrofit object with the Moshi converter.
 */
private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .client(client)
    .build()

object AddClientId : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request: Request = chain.request()
        val url = request.url
            .newBuilder()
            .addQueryParameter("client_id", CLIENT_ID)
            .build()
        request = request.newBuilder().url(url).build()
        return chain.proceed(request)
    }
}

/**
 * A public interface that exposes the [resolve] method
 */
interface SoundCloudApiService {
    /**
     * Returns a [SoundCloudCollection] and this method can be called from a Coroutine.
     * The @GET annotation indicates that the "resolve" endpoint will be requested with the GET
     * HTTP method
     */
    @GET("resolve")
    suspend fun resolve(@Query("url") url: String): SoundCloudCollection

    /**
     * Returns a [Track] and this method can be called from a Coroutine.
     * The @GET annotation indicates that the "tracks" endpoint will be requested with the GET
     * HTTP method
     */
    @GET("tracks/{id}")
    suspend fun track(@Path("id") id: String): Track

    /**
     * Returns a [TrackURL]
     */
    @GET("media/soundcloud:tracks:{id}/{code}/stream/{type}")
    suspend fun media(
        @Path("id") id: String,
        @Path("code") code: String,
        @Path("type") type: String
    ): TrackURL


    @GET("search")
    suspend fun search(
        @Query("limit") limit: Int,
        @Query("q") query: String,
        //@Query("access") access: String = "playable",
    ): SoundCloudSearchResponse

    @GET("featured_tracks/top/all-music")
    suspend fun allMusic(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
    ): SoundCloudSearchResponse
}

/**
 * A public Api object that exposes the lazy-initialized Retrofit service
 */
object SoundCloudApi {
    val retrofitService: SoundCloudApiService by lazy { retrofit.create(SoundCloudApiService::class.java) }
}
