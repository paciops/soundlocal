package it.unipi.di.sam.pacioni.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import it.unipi.di.sam.pacioni.database.TrackPlayed
import it.unipi.di.sam.pacioni.database.TrackPlayedDatabaseDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HistoryViewModel(private val db: TrackPlayedDatabaseDao) : ViewModel() {
    val tracks = MutableLiveData<List<TrackPlayed>>()

    fun getAll() {
        viewModelScope.launch {
            tracks.value = getAllFromDatabase()
        }
    }

    private suspend fun getAllFromDatabase(): List<TrackPlayed> = withContext(Dispatchers.IO) {
        db.getAll()
    }

    fun addTrack(title: String, author: String, mediaId: Int, duration: String, imageUri: String="") {
        viewModelScope.launch {
            val trackPlayed = TrackPlayed(
                title = title,
                author = author,
                mediaId = mediaId,
                duration = duration,
                imageUri = imageUri
            )
            insert(trackPlayed)
        }
    }

    private suspend fun insert(trackPlayed: TrackPlayed) = withContext(Dispatchers.IO) { db.insert(trackPlayed) }
}
