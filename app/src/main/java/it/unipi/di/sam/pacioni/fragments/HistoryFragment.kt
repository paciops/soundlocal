package it.unipi.di.sam.pacioni.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.unipi.di.sam.pacioni.R
import it.unipi.di.sam.pacioni.TracksPlayedAdapter
import it.unipi.di.sam.pacioni.database.TrackPlayedDatabase
import it.unipi.di.sam.pacioni.viewmodels.HistoryViewModel

class HistoryFragment : Fragment() {
    private lateinit var viewModel: HistoryViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val database = TrackPlayedDatabase.getInstance(requireContext()).trackPlayedDatabaseDao
        viewModel = HistoryViewModel(database)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rv = view.findViewById<RecyclerView>(R.id.rvTracksPlayed)
        val adapter = TracksPlayedAdapter()
        rv.let {
            it.layoutManager = LinearLayoutManager(context)
            it.adapter = adapter
        }
        viewModel.getAll()
        viewModel.tracks.observe(viewLifecycleOwner) { adapter.submitList(it) }
    }
}