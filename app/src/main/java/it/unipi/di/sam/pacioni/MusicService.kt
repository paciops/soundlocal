package it.unipi.di.sam.pacioni

import android.app.Notification
import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaDescriptionCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaSessionCompat
import android.widget.Toast
import androidx.media.MediaBrowserServiceCompat
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.ext.cast.CastPlayer
import com.google.android.exoplayer2.ext.cast.SessionAvailabilityListener
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector
import com.google.android.exoplayer2.ext.mediasession.TimelineQueueNavigator
import com.google.android.exoplayer2.ui.PlayerNotificationManager
import com.google.android.exoplayer2.util.MimeTypes
import com.google.android.gms.cast.framework.CastContext

private const val MEDIA_SESSION_TAG = "MusicService"
private const val DEFAULT_VALUE = "..."

class MusicService : MediaBrowserServiceCompat() {
    private lateinit var player: Player
    private lateinit var notificationManager: NotificationManager
    private var currentPlaylistItems: MutableList<MediaMetadataCompat> = arrayListOf()

    private lateinit var mediaSession: MediaSessionCompat
    private lateinit var mediaSessionConnector: MediaSessionConnector

    private val exoPlayer: ExoPlayer by lazy {
        val audioAttributes = AudioAttributes.Builder()
            .setUsage(C.USAGE_MEDIA)
            .setContentType(C.CONTENT_TYPE_SPEECH)
            .build()
        SimpleExoPlayer.Builder(this).build().apply {
            setHandleAudioBecomingNoisy(true)
            setAudioAttributes(audioAttributes, true)
        }
    }

    private val castPlayer: CastPlayer? by lazy {
        try {
            val castContext = CastContext.getSharedInstance(this)
            CastPlayer(castContext).apply {
                setSessionAvailabilityListener(CastSessionAvailabilityListener())
            }
        } catch (e : Exception) {
            // calling `CastContext.getSharedInstance` can throw various exceptions, all of which
            // indicate that Cast is unavailable.
            Toast.makeText(this, "Cast unavailable "+e.message, Toast.LENGTH_SHORT).show()
            null
        }
    }

    companion object {
        const val INTENT_KEY_TITLE = "TITLE"
        const val INTENT_KEY_URI = "URI"
        const val INTENT_KEY_ART_URI = "ART_URI"
        const val INTENT_KEY_AUTHOR = "AUTHOR"
        const val INTENT_KEY_MIMETYPE = "MIME_TYPE"
        const val INTENT_KEY_HLS = "HLS"
        const val INTENT_KEY_MEDIA_ID = "MEDIA_ID"
        const val INTENT_KEY_ACTION = "ACTION"
    }

    override fun onCreate() {
        super.onCreate()
        val sessionActivityPendingIntent =
            packageManager?.getLaunchIntentForPackage(packageName)?.let { sessionIntent ->
                PendingIntent.getActivity(this, 42, sessionIntent, 0)
            }
        mediaSession = MediaSessionCompat(this, MEDIA_SESSION_TAG).apply {
            setSessionActivity(sessionActivityPendingIntent)
            isActive = true
        }

        sessionToken = mediaSession.sessionToken
        notificationManager = NotificationManager(
            this,
            mediaSession.sessionToken,
            PlayerNotificationListener()
        )

        mediaSessionConnector = MediaSessionConnector(mediaSession).apply {
            setQueueNavigator(object : TimelineQueueNavigator(mediaSession) {
                override fun getMediaDescription(
                    player: Player,
                    windowIndex: Int
                ): MediaDescriptionCompat {
                    return if (currentPlaylistItems.isEmpty())
                        MediaDescriptionCompat.Builder()
                            .setTitle(DEFAULT_VALUE)
                            .build()
                    else
                        currentPlaylistItems[windowIndex].description
                }
            })
        }
        switchToPlayer(null,if (castPlayer?.isCastSessionAvailable == true) castPlayer!! else exoPlayer)
        notificationManager.showNotificationForPlayer(player)
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val mime = intent.extras!!.getString(INTENT_KEY_MIMETYPE)
        val hls = intent.extras!!.getBoolean(INTENT_KEY_HLS)
        val action = intent.extras!!.getString(INTENT_KEY_ACTION)
        val uri = intent.extras!!.getString(INTENT_KEY_URI)
        val mediaMetadataCompat = createMediaMetadataCompat(intent)
        val mediaItem: MediaItem = MediaItem.Builder().apply {
            setUri(uri)
            if (hls) setMimeType(MimeTypes.APPLICATION_M3U8) else setMimeType(mime)
            setMediaMetadata(createMediaMetadata(mediaMetadataCompat))
        }.build()
        Toast.makeText(this, action, Toast.LENGTH_SHORT).show()
        when(action) {
            "PLAY" -> {
                player.setMediaItem(mediaItem)
                currentPlaylistItems.clear()
            }
            "ADD" -> player.addMediaItem(mediaItem)
        }
        currentPlaylistItems.add(mediaMetadataCompat)
        player.prepare()
        player.playWhenReady = true
        return START_REDELIVER_INTENT
    }

    private fun createMediaMetadataCompat(intent: Intent?): MediaMetadataCompat {
        val result = MediaMetadataCompat.Builder()
        intent?.let {
            result.apply {
                putString(
                    MediaMetadataCompat.METADATA_KEY_TITLE,
                    intent.extras?.getString(INTENT_KEY_TITLE) ?: DEFAULT_VALUE
                )
                putString(
                    MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON_URI,
                    intent.extras?.getString(INTENT_KEY_ART_URI) ?: ""
                )
                putString(
                    MediaMetadataCompat.METADATA_KEY_AUTHOR,
                    intent.extras?.getString(INTENT_KEY_AUTHOR) ?: DEFAULT_VALUE
                )
                putString(
                    MediaMetadataCompat.METADATA_KEY_MEDIA_ID,
                    intent.extras?.getInt(INTENT_KEY_MEDIA_ID).toString()
                )
            }
        }
        return result.build()
    }

    private fun createMediaMetadata(mmc: MediaMetadataCompat) = MediaMetadata.Builder().apply {
        setTitle(mmc.description.title)
        setArtist(mmc.description.subtitle)
        setMediaUri(player.mediaMetadata.mediaUri)
        setSubtitle(mmc.description.subtitle)
    }.build()

    override fun onDestroy() {
        stopForeground(false)
        notificationManager.hideNotification()
        mediaSession.release()
        mediaSessionConnector.setPlayer(null)
        player.release()
    }

    override fun onGetRoot(clientPackageName: String, clientUid: Int, rootHints: Bundle?): BrowserRoot = BrowserRoot("media_root_id", null)

    override fun onLoadChildren(
        parentId: String,
        result: Result<MutableList<MediaBrowserCompat.MediaItem>>
    ) {
        result.sendResult(emptyList<MediaBrowserCompat.MediaItem>() as MutableList<MediaBrowserCompat.MediaItem>?)
    }

    private inner class PlayerNotificationListener :
        PlayerNotificationManager.NotificationListener {
        override fun onNotificationCancelled(notificationId: Int, dismissedByUser: Boolean) {
            super.onNotificationCancelled(notificationId, dismissedByUser)
            stopForeground(true)
        }

        override fun onNotificationPosted(
            notificationId: Int,
            notification: Notification,
            ongoing: Boolean
        ) {
            super.onNotificationPosted(notificationId, notification, ongoing)
            startForeground(notificationId, notification)
        }
    }

    private inner class CastSessionAvailabilityListener : SessionAvailabilityListener {
        /**
         * Called when a Cast session has started and the user wishes to control playback on a
         * remote Cast receiver rather than play audio locally.
         */
        override fun onCastSessionAvailable() {
            switchToPlayer(player, castPlayer!!)
        }

        /**
         * Called when a Cast session has ended and the user wishes to control playback locally.
         */
        override fun onCastSessionUnavailable() {
            switchToPlayer(player, exoPlayer)
        }
    }

    private fun switchToPlayer(old: Player?, new: Player) {
        if (old == new) {
            return
        }
        player = new
        mediaSessionConnector.setPlayer(new)
        old?.release()
    }
}
