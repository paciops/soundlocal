package it.unipi.di.sam.pacioni

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import it.unipi.di.sam.pacioni.database.TrackPlayed
import it.unipi.di.sam.pacioni.network.Kind
import it.unipi.di.sam.pacioni.network.SoundCloudCollection
import it.unipi.di.sam.pacioni.utils.Utility
import java.text.DateFormat
import java.time.LocalTime

class TracksAdapter(
    private var dataSet: List<SoundCloudCollection>,
    private val listener: (SoundCloudCollection) -> Unit
) : RecyclerView.Adapter<TrackViewHolder>() {

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): TrackViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.track_item, viewGroup, false)
        return TrackViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(trackViewHolder: TrackViewHolder, position: Int) {
        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        val track = dataSet[position]
        trackViewHolder.trackTitle.text = track.title
        trackViewHolder.trackAuthor.text = track.user?.username ?: ""
        val duration = LocalTime.ofSecondOfDay((track.duration?.toLong()?.div(1000) ?: 0))
        trackViewHolder.trackDuration.text = duration.toString()
        trackViewHolder.trackGenre.text = track.genre
        Utility.loadImage(trackViewHolder.trackImage, track.artworkUrl, trackViewHolder.trackImage)
        trackViewHolder.itemView.setOnClickListener { listener(track) }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

    fun setItems(newSet: List<SoundCloudCollection>) {
        dataSet = newSet
    }
}

class SearchAdapter(
    private var dataSet: List<SoundCloudCollection>,
    private val listener: (SoundCloudCollection) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount() = dataSet.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = dataSet[position]
        var time = ""
        try {
            time = LocalTime.ofSecondOfDay((item.duration?.toLong() ?: 0L).div(1000)).toString()
        } catch (e: Exception) {
            Log.d("SearchAdapter.onBindViewHolder", "ID: ${item.id}")
            Log.d("SearchAdapter.onBindViewHolder", e.toString())
        }
        when (item.kind) {
            Kind.user -> {
                holder as UserViewHolder
                holder.apply {
                    userName.text = item.username
                    userCity.text = item.city
                    userFollowers.text = "${item.followersCount} followers"
                    Utility.loadImage(userImage, item.avatarUrl, userImage)
                }
            }
            Kind.track -> {
                holder as TrackViewHolder
                holder.apply {
                    trackTitle.text = item.title
                    trackAuthor.text = item.user!!.username
                    trackGenre.text = item.genre?.uppercase()
                    trackDuration.text = time
                    Utility.loadImage(trackImage, item.artworkUrl, trackImage)
                }
            }
            Kind.playlist -> {
                holder as PlaylistViewHolder
                holder.apply {
                    playlistTitle.text = item.title
                    playlistAuthor.text = item.user!!.username
                    playlistTracks.text = "${item.trackCount} tracks"
                    playlistDuration.text = time
                    Utility.loadImage(playlistImage, item.artworkUrl, playlistImage)
                }
            }
            else -> throw NotImplementedError()
        }
        holder.itemView.setOnClickListener { listener(item) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            Kind.user.i -> UserViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.user_item, parent, false)
            )
            Kind.track.i -> TrackViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.track_item, parent, false)
            )
            Kind.playlist.i -> PlaylistViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.playlist_item, parent, false)
            )
            else -> throw NotImplementedError()
        }
    }

    fun setItems(newSet: List<SoundCloudCollection>) {
        dataSet = newSet
    }

    override fun getItemViewType(position: Int): Int = dataSet[position].kind?.i ?: -1
}

class TracksPlayedAdapter() : ListAdapter<TrackPlayed, TrackViewHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.track_item, parent, false)
        return TrackViewHolder(view)
    }

    override fun onBindViewHolder(holder: TrackViewHolder, position: Int) {
        val trackPlayed  = getItem(position)
        holder.trackTitle.text = trackPlayed.title
        holder.trackAuthor.text = trackPlayed.author
        holder.trackDuration.text = DateFormat.getDateTimeInstance().format(trackPlayed.playedTimeMillis)
        Utility.loadImage(holder.trackImage, trackPlayed.imageUri, holder.trackImage)
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<TrackPlayed>() {
            override fun areItemsTheSame(oldItem: TrackPlayed, newItem: TrackPlayed) = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: TrackPlayed, newItem: TrackPlayed) = oldItem == newItem
        }
    }
}