package it.unipi.di.sam.pacioni

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val userImage: ImageView = view.findViewById(R.id.item_user_image)
    var userName: TextView = view.findViewById(R.id.item_user_name)
    val userCity: TextView = view.findViewById(R.id.item_user_city)
    val userFollowers: TextView = view.findViewById(R.id.item_user_followers)
}

class PlaylistViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val playlistTitle: TextView = view.findViewById(R.id.item_playlist_title)
    val playlistImage: ImageView = view.findViewById(R.id.item_playlist_image)
    val playlistDuration: TextView = view.findViewById(R.id.item_playlist_duration)
    val playlistAuthor: TextView = view.findViewById(R.id.item_playlist_author)
    val playlistTracks: TextView = view.findViewById(R.id.item_playlist_tracks)
}

class TrackViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val trackTitle: TextView = view.findViewById(R.id.item_track_title)
    val trackImage: ImageView = view.findViewById(R.id.item_track_image)
    val trackAuthor: TextView = view.findViewById(R.id.item_track_author)
    val trackDuration: TextView = view.findViewById(R.id.Item_track_duration)
    val trackGenre: TextView = view.findViewById(R.id.item_track_genre)
}