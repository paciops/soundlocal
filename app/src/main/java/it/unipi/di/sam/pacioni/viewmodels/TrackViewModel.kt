package it.unipi.di.sam.pacioni.viewmodels

import android.os.Handler
import android.os.Looper
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.PlaybackStateCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import it.unipi.di.sam.pacioni.network.SoundCloudApi
import it.unipi.di.sam.pacioni.utils.Utility
import it.unipi.di.sam.pacioni.utils.Utility.currentPlayBackPosition
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private const val INTERVAL = 500L

class TrackViewModel : ViewModel(){

    private var updatePosition = true
    private val handler = Handler(Looper.getMainLooper())

    val mediaMetadata = MutableLiveData<MediaMetadataCompat>()
    val playbackState = MutableLiveData<PlaybackStateCompat>()
    val mediaPosition = MutableLiveData(0L).apply {
        checkPlaybackPosition()
    }

    private fun checkPlaybackPosition(): Boolean = handler.postDelayed({
        val currPosition = playbackState.value?.currentPlayBackPosition?:0L
        if (mediaPosition.value != currPosition)
            mediaPosition.postValue(currPosition)
        if(updatePosition) checkPlaybackPosition()
    }, INTERVAL)

    suspend fun getMedia(id: Int?, url: String): String {
        val code = Utility.extractMediaCode(url)
        val type = Utility.extractMediaType(url)
        return withContext(Dispatchers.IO) {
            SoundCloudApi.retrofitService.media(id.toString(), code, type).url
        }
    }

    override fun onCleared() {
        super.onCleared()
        // Stop updating the position
        updatePosition = false
    }
}