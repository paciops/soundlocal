package it.unipi.di.sam.pacioni.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentResultListener
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import it.unipi.di.sam.pacioni.MusicService
import it.unipi.di.sam.pacioni.R
import it.unipi.di.sam.pacioni.network.Kind
import it.unipi.di.sam.pacioni.network.SoundCloudCollection
import it.unipi.di.sam.pacioni.utils.Utility
import it.unipi.di.sam.pacioni.viewmodels.TrackViewModel
import kotlinx.coroutines.launch

class TrackFragment : Fragment(), FragmentResultListener, View.OnClickListener {
    private var track: SoundCloudCollection? = null
    private lateinit var playButton: ImageButton
    private lateinit var addButton: ImageButton
    private val trackViewModel: TrackViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        parentFragmentManager.setFragmentResultListener("BUNDLE", this, this)
        track = savedInstanceState?.getParcelable(Kind.track.toString()) as SoundCloudCollection?
    }

    override fun onResume() {
        super.onResume()
        setTrackInfo()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_track, container, false)
        playButton = rootView.findViewById(R.id.play_button)
        addButton = rootView.findViewById(R.id.add_button)
        playButton.setOnClickListener(this)
        addButton.setOnClickListener(this)
        return rootView
    }

    override fun onFragmentResult(requestKey: String, result: Bundle) {
        result.get(Kind.track.toString())?.let {
            track = it as SoundCloudCollection
            setTrackInfo()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(Kind.track.toString(), track)
    }

    override fun onClick(v: View?) {
        when (v) {
            playButton -> {
                track?.let {
                    viewLifecycleOwner.lifecycleScope.launch {
                        val intent = createIntent(it, "PLAY")
                        context?.startForegroundService(intent)
                    }
                }
            }
            addButton -> {
                track?.let {
                    viewLifecycleOwner.lifecycleScope.launch {
                        val intent = createIntent(it, "ADD")
                        context?.startForegroundService(intent)
                    }
                }
            }
        }
    }

    private suspend fun createIntent(track: SoundCloudCollection, action: String) : Intent {
        val transcoding = Utility.chooseTranscoding(track.media!!.transcodings)
        val url = transcoding.url
        val mime = transcoding.format.mimeType
        val uri = trackViewModel.getMedia(track.id, url)
        val hls = transcoding.format.protocol == "hls"
        return Intent(context, MusicService::class.java).apply {
            putExtra(MusicService.INTENT_KEY_ACTION, action)
            putExtra(MusicService.INTENT_KEY_URI, uri)
            putExtra(MusicService.INTENT_KEY_TITLE, track.title)
            putExtra(MusicService.INTENT_KEY_ART_URI, track.artworkUrl)
            putExtra(MusicService.INTENT_KEY_AUTHOR, track.user?.username)
            putExtra(MusicService.INTENT_KEY_MIMETYPE, mime)
            putExtra(MusicService.INTENT_KEY_HLS, hls)
            putExtra(MusicService.INTENT_KEY_MEDIA_ID, track.id)
        }
    }

    private fun setTrackInfo() {
        track?.let { it ->
            val rv = requireView()
            val description = rv.findViewById<TextView>(R.id.track_description)
            val author = rv.findViewById<TextView>(R.id.track_author)
            val duration = rv.findViewById<TextView>(R.id.track_duration)
            val image = rv.findViewById<ImageView>(R.id.track_image)
            val title = rv.findViewById<TextView>(R.id.track_title)
            val genre = rv.findViewById<TextView>(R.id.track_genre)
            Utility.loadImage(this, it.artworkUrl, image)
            author.text = it.user?.username ?: ""
            description.text = it.description
            duration.text = Utility.convertDuration(it.duration)
            title.text = it.title
            genre.text = it.genre ?: ""
        }
    }
}