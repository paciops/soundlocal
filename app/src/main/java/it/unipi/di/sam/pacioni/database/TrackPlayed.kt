package it.unipi.di.sam.pacioni.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "track_played_table")
data class TrackPlayed (
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L,

    @ColumnInfo(name = "played_time_millis")
    val playedTimeMillis: Long = System.currentTimeMillis(),

    @ColumnInfo(name = "title")
    val title : String,

    @ColumnInfo(name = "author")
    val author : String,

    @ColumnInfo(name = "media_id")
    val mediaId : Int,

    @ColumnInfo(name = "duration")
    val duration : String,

    @ColumnInfo(name = "image_uri")
    val imageUri: String = "",
)