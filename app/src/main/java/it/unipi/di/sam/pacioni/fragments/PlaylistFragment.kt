package it.unipi.di.sam.pacioni.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentResultListener
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.unipi.di.sam.pacioni.R
import it.unipi.di.sam.pacioni.TracksAdapter
import it.unipi.di.sam.pacioni.network.Kind
import it.unipi.di.sam.pacioni.network.SoundCloudCollection
import it.unipi.di.sam.pacioni.network.Track
import it.unipi.di.sam.pacioni.utils.Utility

class PlaylistFragment : Fragment(), FragmentResultListener {
    private var playlist: SoundCloudCollection? = null
    private lateinit var rvTracks: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_playlist, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        parentFragmentManager.setFragmentResultListener("BUNDLE", this, this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvTracks = view.findViewById(R.id.rvPlaylistTracks)
    }

    override fun onFragmentResult(requestKey: String, result: Bundle) {
        result.get(Kind.playlist.toString())?.let {
            playlist = it as SoundCloudCollection
            setPlaylistInfo()
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun setPlaylistInfo() {
        playlist?.let {
            val rv = requireView()
            val image = rv.findViewById<ImageView>(R.id.playlist_image)
            val track_count = rv.findViewById<TextView>(R.id.playlist_tracks_count)
            val title = rv.findViewById<TextView>(R.id.playlist_title)
            val duration = rv.findViewById<TextView>(R.id.playlist_duration)
            val author = rv.findViewById<TextView>(R.id.playlist_author)
            val description = rv.findViewById<TextView>(R.id.playlist_description)

            Utility.loadImage(this, it.artworkUrl, image)
            track_count.text = it.trackCount?.toString() ?: "0"
            title.text = it.title
            duration.text = Utility.convertDuration(it.duration)
            author.text = it.user?.username ?: ""
            description.text = it.description
            it.tracks?.let { tracks ->
                val adapter = TracksAdapter(tracks){}
                rvTracks.adapter = adapter
                rvTracks.layoutManager = LinearLayoutManager(requireContext())
            }
        }
    }
}