package it.unipi.di.sam.pacioni

import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import it.unipi.di.sam.pacioni.database.TrackPlayedDatabase
import it.unipi.di.sam.pacioni.fragments.HistoryFragment
import it.unipi.di.sam.pacioni.fragments.HomeFragment
import it.unipi.di.sam.pacioni.fragments.PlayerFragment
import it.unipi.di.sam.pacioni.fragments.SearchFragment
import it.unipi.di.sam.pacioni.viewmodels.HistoryViewModel
import it.unipi.di.sam.pacioni.viewmodels.TrackViewModel

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    private lateinit var bottomNavigationView: BottomNavigationView
    private lateinit var mediaBrowser : MediaBrowserCompat

    private val homeFragment = HomeFragment()
    private val searchFragment = SearchFragment()
    private val playerFragment = PlayerFragment()
    private val historyFragment = HistoryFragment()

    private val trackModel: TrackViewModel by viewModels()
    private lateinit var historyViewModel: HistoryViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mediaBrowser = MediaBrowserCompat(
            this,
            ComponentName(this, MusicService::class.java),
            connectionCallbacks,
            null )
            .apply { connect() }
        bottomNavigationView = findViewById(R.id.bottomNavigationView)
        setCurrentFragment(homeFragment, "home", false)
        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        val database = TrackPlayedDatabase.getInstance(applicationContext).trackPlayedDatabaseDao
        historyViewModel = HistoryViewModel(database)
    }

    private val connectionCallbacks = object : MediaBrowserCompat.ConnectionCallback() {
        override fun onConnected() {
            super.onConnected()
            mediaBrowser.sessionToken.also {
                val mediaController = MediaControllerCompat(this@MainActivity, it)
                    .apply { registerCallback(controllerCallback) }
                MediaControllerCompat.setMediaController(this@MainActivity, mediaController)
            }
        }

        override fun onConnectionSuspended() {
            Toast.makeText(this@MainActivity, "Connection suspended", Toast.LENGTH_SHORT).show()
        }

        override fun onConnectionFailed() {
            Toast.makeText(this@MainActivity, "Connection failed", Toast.LENGTH_SHORT).show()
        }
    }

    private var controllerCallback = object : MediaControllerCompat.Callback() {
        private var prev : String? = null
        override fun onMetadataChanged(metadata: MediaMetadataCompat?) {
            metadata?.let {
                if (prev != it.description.mediaId) {
                    prev = it.description.mediaId
                    trackModel.mediaMetadata.value = it
                    historyViewModel.addTrack(
                        it.description.title.toString(),
                        it.description.subtitle.toString(),
                        it.description.mediaId!!.toInt(),
                        "",
                        it.description.iconUri.toString()
                    )
                }
            }
        }

        override fun onPlaybackStateChanged(state: PlaybackStateCompat?) {
            state?.let { trackModel.playbackState.value = it}
        }
    }

    private fun setCurrentFragment(fragment: Fragment, name: String, backStack: Boolean) {
        supportFragmentManager.beginTransaction()
            .apply {
                replace(R.id.flFragment, fragment, name)
                setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                if (backStack) {
                    addToBackStack(name)
                }
                setReorderingAllowed(true)
                commit()
            }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if (!back) {
            when (item.itemId) {
                R.id.miHome -> setCurrentFragment(homeFragment, "home", true)
                R.id.miSearch -> setCurrentFragment(searchFragment, "search", true)
                R.id.miHistory -> setCurrentFragment(historyFragment, "history", true)
                R.id.miPlayer -> setCurrentFragment(playerFragment, "player", true)
            }
        }
        back = false
        return true
    }

    // workaround to set the correct menu button when going backwards
    // if it looks stupid but works, it is not stupid :)
    private var back = false

    override fun onBackPressed() {
        super.onBackPressed()
        val index = supportFragmentManager.backStackEntryCount - 1
        back = true
        if (index >= 0) {
            when (supportFragmentManager.getBackStackEntryAt(index).name) {
                "home" -> bottomNavigationView.selectedItemId = R.id.miHome
                "search" -> bottomNavigationView.selectedItemId = R.id.miSearch
                "history" -> bottomNavigationView.selectedItemId = R.id.miHistory
                "player" -> bottomNavigationView.selectedItemId = R.id.miPlayer
            }
        } else {
            bottomNavigationView.selectedItemId = R.id.miHome
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaBrowser.disconnect()
        stopService(Intent(this, MusicService::class.java))
    }
}
