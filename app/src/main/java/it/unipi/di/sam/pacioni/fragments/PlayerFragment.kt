package it.unipi.di.sam.pacioni.fragments

import android.os.Bundle
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.mediarouter.app.MediaRouteButton
import com.google.android.gms.cast.framework.CastButtonFactory
import it.unipi.di.sam.pacioni.R
import it.unipi.di.sam.pacioni.utils.Utility
import it.unipi.di.sam.pacioni.viewmodels.TrackViewModel

class PlayerFragment : Fragment(), View.OnClickListener {

    private val trackModel: TrackViewModel by activityViewModels()
    private lateinit var mMediaRouteButton: MediaRouteButton
    private lateinit var controlButton : ImageButton
    private lateinit var prevButton : ImageButton
    private lateinit var nextButton : ImageButton

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_player, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val context = requireContext()

        mMediaRouteButton = view.findViewById(R.id.media_route_button)
        controlButton = view.findViewById(R.id.controlButton)
        prevButton = view.findViewById(R.id.previousTrack)
        nextButton = view.findViewById(R.id.nextTrack)
        CastButtonFactory.setUpMediaRouteButton(context, mMediaRouteButton)
        //val castContext = CastContext.getSharedInstance()

        trackModel.mediaMetadata.observe(viewLifecycleOwner) {
            view.findViewById<TextView>(R.id.currentTrack).text = it.description.title
            Utility.loadImage(this@PlayerFragment, it.description.iconUri.toString(), view.findViewById(R.id.currentImage))
        }
        trackModel.mediaPosition.observe(viewLifecycleOwner){ pos ->
            view.findViewById<TextView>(R.id.currentPosition).text = Utility.timestampToMSS(context, pos)
        }
        trackModel.playbackState.observe(viewLifecycleOwner) {
            when(it.state) {
                PlaybackStateCompat.STATE_PAUSED -> controlButton.setImageResource(R.drawable.exo_icon_play)
                PlaybackStateCompat.STATE_BUFFERING,
                PlaybackStateCompat.STATE_PLAYING -> controlButton.setImageResource(R.drawable.exo_icon_pause)
            }
        }
        controlButton.setOnClickListener(this)
        prevButton.setOnClickListener(this)
        nextButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        val mcc = MediaControllerCompat.getMediaController(requireActivity())
        when(v) {
            controlButton -> {
                when(trackModel.playbackState.value?.state) {
                    PlaybackStateCompat.STATE_PLAYING -> mcc.transportControls.pause()
                    PlaybackStateCompat.STATE_PAUSED -> mcc.transportControls.play()
                }
            }
            prevButton -> mcc.transportControls.skipToPrevious()
            nextButton -> mcc.transportControls.skipToNext()
        }
    }
}