package it.unipi.di.sam.pacioni.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentResultListener
import it.unipi.di.sam.pacioni.R
import it.unipi.di.sam.pacioni.network.Kind
import it.unipi.di.sam.pacioni.network.SoundCloudCollection
import it.unipi.di.sam.pacioni.utils.Utility

class UserFragment : Fragment(), FragmentResultListener {
    private var user: SoundCloudCollection? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        parentFragmentManager.setFragmentResultListener("BUNDLE", this, this)
    }

    override fun onFragmentResult(requestKey: String, result: Bundle) {
        result.get(Kind.user.toString())?.let {
            user = it as SoundCloudCollection
            setUserInfo()
        }
    }

    private fun setUserInfo() {
        user?.let { it ->
            val realView = requireView()
            val image = realView.findViewById<ImageView>(R.id.user_image)
            val name = realView.findViewById<TextView>(R.id.user_name)
            val city = realView.findViewById<TextView>(R.id.user_city)
            val followers = realView.findViewById<TextView>(R.id.user_followers)
            val description = realView.findViewById<TextView>(R.id.user_description)

            Utility.loadImage(this, it.avatarUrl, image)
            name.text = it.username
            city.text = it.city ?: ""
            "${it.followersCount ?: 0} followers".also { n -> followers.text = n }
            description.text = it.description
        }
    }
}