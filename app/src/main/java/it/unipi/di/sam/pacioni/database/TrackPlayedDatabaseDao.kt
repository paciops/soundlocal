package it.unipi.di.sam.pacioni.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface TrackPlayedDatabaseDao {

    @Insert
    fun insert(trackPlayed: TrackPlayed)

    @Query("SELECT * FROM track_played_table ORDER BY id DESC")
    fun getAll(): List<TrackPlayed>
}