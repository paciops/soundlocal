package it.unipi.di.sam.pacioni.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(TrackPlayed::class), version = 2, exportSchema = false)
abstract class TrackPlayedDatabase: RoomDatabase(){

    abstract val trackPlayedDatabaseDao: TrackPlayedDatabaseDao

    companion object {
        // For Singleton instantiation.
        @Volatile
        private var instance: TrackPlayedDatabase? = null

        fun getInstance(context: Context) =
            instance ?: synchronized(this) {
                instance ?: Room.databaseBuilder(
                    context,
                    TrackPlayedDatabase::class.java,
                    "track_played_database"
                ).fallbackToDestructiveMigration()
                    .build().also { instance = it }
            }
    }
}